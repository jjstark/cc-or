## `K.CC` Counting and Cardinality 

- Have student write numbers 1-9. If there are any illegible or incorrect characters write 1-9 below the student's numbers and ask the student to circle any incorrect or illegible numbers that they wrote. Once the student had found the problems, ask them to try again with all of this visible. 
  - Addresses
    - `K.CC.3`   
- Have student count to 100 by 10s and write down each number. This visual will be helpful in counting by 1s.
  - Addresses
    - `K.CC.1`    
- Have student count from 1 to 100 and write down any missed numbers. Ask student to being counting again from 5 or so numbers below the missed number and practice counting through it. This should be done with writing the lower number down and asking the student to count from the written number. 
  - Addresses
    - `K.CC.1`
    - `K.CC.2`
- Write two random numbers between 1 and 100, ask the student to identify the greater of the two numbers. Once correctly identified, have the student count from the lesser to the greater number. Cover any ranges or numbers that the student forgets or struggles with in counting. 
  - Addresses
    - `K.CC.1`
    - `K.CC.2`
- Write an array of 10s or other intervals from least to greatest from the top to the bottom of the  side of a paper or board. Write numbers on the other side of the workspace and have the student draw an arrow or line to the space between the numbers in the array where that number would fall
		
		
							10
							- 
							20
							- 
							30
							- <--------------- 34	
							40
							- 
							50 
		
  - Addresses
    - `K.CC.1`
    - `K.CC.2`
    - `K.CC.6`
- Play card cames like war
    - Addresses
        - `K.CC.6`
- Play Go Fish with cards for pairs that add or subtract up to selected numbers. For example, play a round where a pair is a set of two cards that sum to 10. 
    - Addresses
        - `K.OA.2`
        - `K.OA.3`
        - `K.OA.4`
        - `K.OA.5` 
- Play Bingo for number identification and math problems. For example, you could call out "2+4" and the student would mark "6" on their board. 
    - Adresses
        - `K.CC.7`
        - `K.OA.*`
