# Kindergarten Language Arts & Literacy Standards

## Kindergarten Reading Foundational Skills `K.RF`

- `K.RF.1` Demonstrate understanding of the organization and basic features of print.
- `K.RF.1a` Follow words from left to right, top to bottom, and page by page.
- `K.RF.1b` Recognize that spoken words are represented in written language by specific sequences of letters.
- `K.RF.1c` Understand that words are separated by spaces in print.
- `K.RF.1d` Recognize and name all upper- and lowercase letters of the alphabet.

Phonological Awareness
 
- `K.RF.2` Demonstrate understanding of spoken words, syllables, and phonemes.
  - `K.RF.2a` Recognize and produce rhyming words.
  - `K.RF.2b` Count, pronounce, blend, and segment syllables in spoken words.
  - `K.RF.2c` Delete syllables in compound words with two syllables.*
  - `K.RF.2d` Blend and segment onsets and rimes of single-syllable spoken words.*
  - `K.RF.2e` Isolate and pronounce the initial, medial vowel, and final phonemes in three phoneme words.
  - `K.RF.2f` Add, delete, and substitute individual phonemes in simple, one-syllable words to make new words.

Phonics and Word Recognition

- `K.RF.3` Know and apply grade-level phonics and word analysis skills in decoding words.
  - `K.RF.3a` Demonstrate basic knowledge of one-to-one letter-sound correspondences by producing the primary sound and the most frequent sounds for each consonant.
  - `K.RF.3b` Associate the long and short sounds with common spellings for the five major vowels.
  - `K.RF.3c` Read common grade-appropriate high-frequency words by sight.
  - `K.RF.3d` Distinguish between similarly spelled words by identifying the sounds of the letters that differ.
  - `K.RF.3e` Decode cvc words.

Fluency

- `K.RF.4` Read emergent texts to develop fluency and comprehension skills.*
  - `K.RF.4a` Read emergent texts with one-to-one correspondence with purpose and understanding

## Kindergarten Reading Literature `K.RL`

Key Ideas and Details

- `K.RL.1` With prompting and support, ask and answer questions about key details in a text.
- `K.RL.2` With prompting and support, retell familiar stories, including key details.
- `K.RL.3` With prompting and support, identify characters, settings, and major events in a story. Identify beginning/middle/end.

Craft and Structure

- `K.RL.4` With prompting and support, ask and answer questions about unknown words in a text.
- `K.RL.5` Recognize common types of texts.*
- `K.RL.6` With prompting and support, identify the author and illustrator of a story and define the role of each in telling the story.*

Integration of Knowledge and Ideas

- `K.RL.7` With prompting and support, describe the relationship between illustrations and the story in which they appear.*
- `K.RL.8` (Not applicable to literature)
- `K.RL.9` With prompting and support, compare and contrast the adventures and experiences of characters in familiar stories.

Range of Reading and Level of Text Complexity

- `K.RL.10` Actively engage in group reading activities with purpose and understanding.

## Kindergarten Reading Informational Text `K.RI`

Key Ideas and Details

- `K.RI.1` With prompting and support, ask and answer questions about key details in a text.
- `K.RI.2` With prompting and support, identify the main topic and retell key details of a text.
- `K.RI.3` With prompting and support, describe the connection between two individuals, events, ideas, or pieces of information in a text.

Craft and Structure

- `K.RI.4` With prompting and support, ask and answer questions about unknown words in a text.
- `K.RI.5` Identify the front cover, back cover, and title page of a book.
- `K.RI.6` With prompting and support, identify the author and illustrator of a text and define the role of each in presenting the ideas or information in a text.*

Integration of Knowledge and Ideas

- `K.RI.7` With prompting and support, describe the relationship between illustrations and the text in which they appear.*
- `K.RI.8` With prompting and support, identify the reasons an author gives to support points in a text.
- `K.RI.9` With prompting and support, identify basic similarities in and differences between two texts on the same topic.*

Range of Reading and Level of Text Complexity

- `K.RI.10` Actively engage in group reading activities with purpose and understanding.


## Kindergarten Writing `K.W`

Text Types and Purposes

- `K.W.1` Use a combination of drawing, dictating, and writing to compose opinion pieces, in which they tell a reader the topic or the name of the book they are writing about and state an opinion or preference about the topic or book.*
- `K.W.2` Use a combination of drawing, dictating, and writing to compose informative/explanatory texts in which they name what they are writing about and supply some information about the topic.
- `K.W.3` Use a combination of drawing, dictating, and writing to narrate a single event or several loosely linked events, sequence the events in the order in which they occurred, and provide a reaction to what happened.*

Production and Distribution of Writing

- `K.W.4` (Begins in grade 3)
- `K.W.5` With guidance and support, respond to questions and suggestions from peers and add details to strengthen writing as needed.*
- `K.W.6` With guidance and support, explore a variety of digital tools to produce and publish writing, including in collaboration with peers.

Research to Build and Present Knowledge

- `K.W.7` With guidance and support, participate in shared research and writing projects.*
- `K.W.8` With guidance and support, recall information from experiences or gather information from provided sources to answer a question.

## Kindergarten Language `K.L`

Conventions of Standard English

- `K.L.1` Demonstrate command of the conventions of standard English grammar and usage when writing or speaking.
  - `K.L.1a` Print all upper- and lowercase letters.*
  - `K.L.1b` Use frequently occurring nouns and verbs.
  - `K.L.1c` Form regular plural nouns orally by adding /s/ or /es/.*
  - `K.L.1d` Understand and use question words.*
  - `K.L.1e` Use the most frequently occurring prepositions.*
  - `K.L.1f` Produce and expand complete sentences in shared language activities.
- `K.L.2` Demonstrate command of the conventions of standard English capitalization, punctuation, and spelling when writing.
  - `K.L.2a` Capitalize the first word in a sentence and the pronoun I.
  - `K.L.2b` Recognize and name end punctuation.
  - `K.L.2c` Write a letter or letters for all consonant and short-vowel phonemes.*
  - `K.L.2d` Spell simple words phonetically.


## Kindergarten Speaking and Listening `K.SL`

Comprehension and Collaboration

- `K.SL.1` Participate in collaborative conversations with diverse partners about kindergarten topics and texts with peers and adults in small and larger groups.
  - `K.SL.1a` With guidance and support, follow agreed-upon rules for discussions.*
  - `K.SL.1b` Continue conversations through multiple exchanges.
- `K.SL.2` Confirm understanding of a text read aloud or information presented orally or through other media by asking and answering questions about key details and requesting clarification if something is not understood.
- `K.SL.3` Ask and answer questions in order to seek help, get information, or clarify something that is not understood.

Presentation of Knowledge and Ideas

- `K.SL.4` Describe familiar people, places, things, and events and, with prompting and support, provide additional detail.
- `K.SL.5` Add drawings or other visual displays to descriptions as desired to provide additional detail.
- `K.SL.6` Speak audibly and express thoughts, feelings, and ideas clearly.

---

source: https://www.oregon.gov/ode/educator-resources/standards/ELA/Documents/Kindergarten.pdf