# COMMON CORE STATE STANDARDS FOR Mathematics (CCSSM) - Kindergarten

In Kindergarten, instructional time should focus on two critical areas: (1) representing and comparing
whole numbers, initially with sets of objects; (2) describing shapes and space. More learning time in
Kindergarten should be devoted to number than to other topics.

## Critical Area 1

Representing and comparing whole numbers, initially with sets of objects. Students use numbers, including written numerals, to represent quantities and to solve quantitative problems, such as counting objects in a set; counting out a given number of objects; comparing sets or numerals; and modeling simple joining and separating situations with sets of objects, or eventually with equations such as 5 + 2 = 7 and 7 – 2 = 5. (Kindergarten students should see addition and subtraction equations, and student writing of equations in kindergarten is encouraged, but it is not required.) Students choose, combine, and apply effective strategies for answering quantitative questions, including quickly recognizing the cardinalities of small sets of objects, counting and producing sets of given sizes, counting the number of objects in combined sets, or counting the number of objects that remain in a set after some are taken away.

## Critical Area #2

Describing shapes and space. Students describe their physical world using geometric ideas (e.g., shape, orientation, spatial relations) and vocabulary. They identify, name, and describe basic two-dimensional shapes, such as squares, triangles, circles, rectangles, and hexagons, presented in a variety of ways (e.g., with different sizes and orientations as well as three-dimensional shapes such as cubes, cones, cylinders, and spheres. They use basic shapes and spatial reasoning to model objects in their environment and to construct more complex shapes.

## Mathematical Practices `K.MP`

The Standards for Mathematical Practice describe varieties of expertise that mathematics educators at all levels should seek to develop in their students.

- `K.MP.1` Make sense of problems and persevere in solving them.
- `K.MP.2` Reason abstractly and quantitatively.
- `K.MP.3` Construct viable arguments and critique the reasoning of others.
- `K.MP.4` Model with mathematics.
- `K.MP.5` Use appropriate tools strategically.
- `K.MP.6` Attend to precision.
- `K.MP.7` Look for and make use of structure.
- `K.MP.8` Look for and express regularity in repeated reasoning.


## Counting and Cardinality `K.CC`

A. Know number names and the count sequence.

- `K.CC.1` Count to 100 by ones and by tens.
- `K.CC.2` Count forward beginning from a given number within the known sequence (instead of having to begin at 1).
- `K.CC.3` Write numbers from 0 to 20. Represent a number of objects with a written numeral 0-20 (with 0 representing a count of no objects). 

B. Count to tell the number of objects.

- `K.CC.4` Understand the relationship between numbers and quantities; connect counting to cardinality. 
	- `K.CC.4a` When counting objects, say the number names in the standard order, pairing each object with one and only one number name and each number name with one and only one object. 
	- `K.CC.4b` Understand that the last number name said tells the number of objects counted. The number of objects is the same regardless of their arrangement or the order in which they were counted.
	- `K.CC.4c` Understand that each successive number name refers to a quantity that is one larger.
- `K.CC.5` Count to answer “how many?” questions about as many as 20 things arranged in a line, a rectangular array, or a circle, or as many as 10 things in a scattered configuration; given a number from 1–20, count out that many objects. 

C. Compare numbers.
- `K.CC.6` Identify whether the number of objects in one group is greater than, less than, or equal to the number of objects in another group, e.g., by using matching and counting strategies (include groups with up to ten objects). 
- `K.CC.7` Compare two numbers between 1 and 10 presented as written numerals.


## Operations and Algebraic Thinking  `K.OA`

D. Understand addition as putting together and adding to, and understand subtraction as taking apart and taking from.

- `K.OA.1` Represent addition and subtraction with objects, fingers, mental images, drawings (Drawings need not show details, but should show the mathematics in the problem.(This applies wherever drawings are mentioned in the Standards.)), sounds (e.g., claps), acting out situations, verbal explanations, expressions, or equations. 
- `K.OA.2` Solve addition and subtraction word problems, and add and subtract within 10, e.g., by using objects or drawings to represent the problem.
- `K.OA.3` Decompose numbers less than or equal to 10 into pairs in more than one way, e.g., by using objects or drawings, and record each decomposition by a drawing or equation (e.g., 5 = 2 + 3 and 5 = 4 + 1).
- `K.OA.4` For any number from 1 to 9, find the number that makes 10 when added to the given number, e.g., by using objects or drawings, and record the answer with a drawing or equation.
- `K.OA.5` Fluently add and subtract within 5.


## Number and Operations in Base Ten `K.NBT`

E. Work with numbers 11–19 to gain foundations for place value.

- `K.NBT.1` Compose and decompose numbers from 11 to 19 into ten ones and some further ones, e.g., by using objects or drawings, and record each composition or decomposition by a drawing or equation (e.g., 18 = 10 + 8); understand that these numbers are composed of ten ones and one, two, three, four, five, six, seven, eight, or nine ones.

## Measurement and Data `K.MD`

F. Describe and compare measurable attributes.
	
- `K.MD.1` Describe measurable attributes of objects, such as length or weight. Describe several measurable attributes of a single object.
- `K.MD.2` Directly compare two objects with a measurable attribute in common, to see which object has “more of”/“less of” the attribute, and describe the difference. For example, directly compare the heights of two children and describe one child as taller/shorter.

G. Classify objects and count the number of objects in each category.

- `K.MD.3` Classify objects into given categories; count the numbers of objects in each category and sort the categories by count. (Limit category counts to be less than or equal to 10.)

## Geometry  `K.G`

H. Identify and describe shapes (squares, circles, triangles, rectangles, hexagons, cubes, cones, cylinders, and spheres.

- `K.G.1` Describe objects in the environment using names of shapes, and describe the relative positions of these objects using terms such as above, below, beside, in front of, behind, and next to.
- `K.G.2` Correctly name shapes regardless of their orientations or overall size.
- `K.G.3` Identify shapes as two-dimensional (lying in a plane, “flat”) or three-dimensional (“solid”).

I. Analyze, compare, create, and compose shapes.

- `K.G.4` Analyze and compare two- and three-dimensional shapes, in different sizes and orientations, using informal language to describe their similarities, differences, parts (e.g., number of sides and vertices/“corners”) and other attributes (e.g., having sides of equal length).
- `K.G.5` Model shapes in the world by building shapes from components (e.g., sticks and clay balls) and drawing shapes.
- `K.G.6` Compose simple shapes to form larger shapes. For example, “Can you join these two triangles with full sides touching to make a rectangle?”

---

source: https://www.oregon.gov/ode/educator-resources/standards/mathematics/Documents/ccssmk.pdf