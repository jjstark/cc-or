# Remote Learning Standards and Resources

Standards and Activities are organized by grade and subject

- [Kindergarten](https://gitlab.com/jjstark/cc-or/-/tree/master/kindergarten)

General Learning Resources are here in the root directory

- [Remote Learning Resources](https://gitlab.com/jjstark/cc-or/-/blob/master/resources.md) 
