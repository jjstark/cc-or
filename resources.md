# Remote Learning Resources

## General

- [Scholastic Learn at Home](https://classroommagazines.scholastic.com/support/learnathome.html) 
- [khanacademy.org](https://www.khanacademy.org/)

## PE

- [Cosmic Kids Yoga](https://www.youtube.com/user/CosmicKidsYoga) 