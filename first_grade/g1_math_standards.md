# COMMON CORE STATE STANDARDS FOR Mathematics (CCSSM) - Grade 1

In Grade 1, instructional time should focus on four critical areas: (1) developing understanding of addition,
subtraction, and strategies for addition and subtraction within 20; (2) developing understanding of whole
number relationships and place value, including grouping in tens and ones; (3) developing understanding of
linear measurement and measuring lengths as iterating length units; and (4) reasoning about attributes of,
and composing and decomposing geometric shapes.

## Critical Area 1

Developing understanding of addition, subtraction, and strategies for addition and subtraction within 20. Students develop strategies for adding and subtracting whole numbers based on their prior work with small numbers. They use a variety of models, including discrete objects and length-based models (e.g., cubes connected to form lengths), to model add-to, take-from, put-together, take-apart, and compare situations to develop meaning for the operations of addition and subtraction, and to develop strategies to solve arithmetic problems with these operations. Students understand connections between counting and addition and subtraction (e.g., adding two is the same as counting on two). They use properties of addition to add whole numbers and to create and use increasingly sophisticated strategies based on these properties (e.g., “making tens”) to solve addition and subtraction problems within 20. By comparing a variety of solution strategies, children build their understanding of the relationship between addition and subtraction.

## Critical Area 2

Developing understanding of whole number relationships and place value, including grouping in tens and ones. Students develop, discuss, and use efficient, accurate, and generalizable methods to add within 100 and subtract multiples of 10. They compare whole numbers (at least to 100) to develop understanding of and solve problems involving their relative sizes. They think of whole numbers between 10 and 100 in terms of tens and ones (especially recognizing the numbers 11 to 19 as composed of a ten and some ones). Through activities that build number sense, they understand the order of the counting numbers and their relative magnitudes.

## Critical Area 3

Developing understanding of linear measurement and measuring lengths as iterating length units. Students develop an understanding of the meaning and processes of measurement, including underlying concepts such as iterating (the mental activity of building up the length of an object with equal-sized units) and the transitivity principle for indirect measurement.(Students should apply the principle of transitivity of measurement to make indirect comparisons, but they need not use this technical term.)

## Critical Area 4
Reasoning about attributes of, and composing and decomposing geometric shapes. Students compose and decompose plane or solid figures (e.g., put two triangles together to make a quadrilateral) and build understanding of part-whole relationships as well as the properties of the original and composite shapes. As they combine shapes, they recognize them from different perspectives and orientations, describe their geometric attributes, and determine how they are alike and different, to develop the background for measurement and for initial understandings of properties such as congruence and symmetry.

## How to read the grade level standards


- *Standards* define what students should understand and be able to do.
- **Clusters** are groups of related standards. Note that standards from different clusters may sometimes be closely related, because mathematics is a connected subject.
- **Domains** are larger groups of related standards. Standards from different domains may sometimes be closely related.

## Grade 1 Overview

**Operations and Algebraic Thinking OA**
- A. Represent and solve problems involving addition and subtraction.
- B. Understand and apply properties of operations and the relationship between addition and subtraction.
- C. Add and subtract within 20.
- D. Work with addition and subtraction equations.

**Number and Operations in Base Ten NBT**
- E. Extend the counting sequence.
- F. Understand place value.
- G. Use place value understanding and properties of operations to add and subtract.
 
**Measurement and Data MD**
- H. Measure lengths indirectly and by iterating length units.
- I. Tell and write time.
- J. Represent and interpret data.

**Geometry G**
- K. Reason with shapes and their attributes.

## Mathematical Practices 1 .MP

The Standards for Mathematical Practice describe varieties of expertise that mathematics educators at all
levels should seek to develop in their students.

- `1.MP.1` Make sense of problems and persevere in solving them.
- `1.MP.2` Reason abstractly and quantitatively.
- `1.MP.3` Construct viable arguments and critique the reasoning of others.
- `1.MP.4` Model with mathematics.
- `1.MP.5` Use appropriate tools strategically.
- `1.MP.6` Attend to precision.
- `1.MP.7` Look for and make use of structure.
- `1.MP.8` Look for and express regularity in repeated reasoning.

## Operations and Algebraic Thinking `1.OA`

A. Represent and solve problems involving addition and subtraction._

- `1.OA.1` Use addition and subtraction within 20 to solve word problems involving situations of adding to, taking from, putting together, taking apart, and comparing, with unknowns in all positions, e.g., by using objects, drawings, and equations with a symbol for the unknown number to represent the problem. (See Glossary)
- `1.OA.2` Solve word problems that call for addition of three whole numbers whose sum is less than or equal to 20, e.g., by using objects, drawings, and equations with a symbol for the unknown number to represent the problem.

B. Understand and apply properties of operations and the relationship between addition and subtraction.

- `1.OA.3` Apply properties of operations as strategies to add and subtract. (Students need not use formal terms for these properties.)
- `1.OA.4` Understand subtraction as an unknown-addend problem. _For example, subtract 10 – 8 by finding the number that makes 10 when added to 8.

C. Add and subtract within 20.

- `1.OA.5` Relate counting to addition and subtraction (e.g., by counting on 2 to add 2).
-  `1.OA.6` Add and subtract within 20, demonstrating fluency for addition and subtraction within 10. Use strategies such as counting on; making ten (e.g., 8 + 6 = 8 + 2 + 4 = 10 + 4 = 14); decomposing a number leading to a ten (e.g., 13 – 4 = 13 – 3 – 1 = 10 – 1 = 9); using the relationship between addition and subtraction (e.g., knowing that 8 + 4 = 12, one knows 12 – 8 = 4); and creating equivalent but easier or known sums (e.g., adding 6 + 7 by creating the known equivalent 6 + 6 + 1 = 12 + 1 = 13).

D. Work with addition and subtraction equations.

- `1.OA.7` Understand the meaning of the equal sign, and determine if equations involving addition and subtraction are true or false. For example, which of the following equations are true and which are false? 6 = 6, 7 = 8 – , 5 + 2 = 2 + 5, 4 + 1 = 5 + 2.
- `1.OA.8` Determine the unknown whole number in an addition or subtraction equation relating to three whole numbers.

## Number and Operations in Base Ten `1.NBT`

E. Extend the counting sequence.

- `1.NBT.1` Count to 120, starting at any number less than 120. In this range, read and write numerals and represent a number of objects with a written numeral.

F. Understand place value.

- `1.NBT.2` Understand that the two digits of a two-digit number represent amounts of tens and ones. Understand the following as special cases:
  - `1.NBT.2a` 10 can be thought of as a bundle of ten ones — called a “ten.”
  - `1.NBT.2b` The numbers from 11 to 19 are composed of a ten and one, two, three, four, five, six, seven, eight, or nine ones.
  - `1.NBT.2c` The numbers 10, 20, 30, 40, 50, 60, 70, 80, 90 refer to one, two, three, four, five, six, seven, eight, or nine tens (and 0 ones).
- `1.NBT.3` Compare two two-digit numbers based on meanings of the tens and ones digits, recording the results of comparisons with the symbols >, =, and <.

G. Use place value understanding and properties of operations to add and subtract.

- `1.NBT.4` Add within 100, including adding a two-digit number and a one-digit number, and adding a two- digit number and a multiple of 10, using concrete models or drawings and strategies based on place value, properties of operations, and/or the relationship between addition and subtraction; relate the strategy to a written method and explain the reasoning used. Understand that in adding two-digit numbers, one adds tens and tens, ones and ones; and sometimes it is necessary to compose a ten.
- `1.NBT.5` Given a two-digit number, mentally find 10 more or 10 less than the number, without having to count; explain the reasoning used.
- `1.NBT.6` Subtract multiples of 10 in the range 10-90 from multiples of 10 in the range 10-90 (positive or zero differences), using concrete models or drawings and strategies based on place value, properties of operations, and/or the relationship between addition and subtraction; relate the strategy to a written method and explain the reasoning used.

## Measurement and Data `1.MD`

H. Measure lengths indirectly and by iterating length units.

- `1.MD.1` Order three objects by length; compare the lengths of two objects indirectly by using a third object.
- `1.MD.2` Express the length of an object as a whole number of length units, by laying multiple copies of a shorter object (the length unit) end to end; understand that the length measurement of an object is the number of same-size length units that span it with no gaps or overlaps. _Limit to contexts where the object being measured is spanned by a whole number of length units with no gaps or overlaps._

I. Tell and write time.

- `1.MD.3` Tell and write time in hours and half-hours using analog and digital clocks.

J. Represent and interpret data.

- `1.MD.4` Organize, represent, and interpret data with up to three categories; ask and answer questions about the total number of data points, how many in each category, and how many more or less are in one category than in another.

## Geometry `1.G`

K. Reason with shapes and their attributes.

- `1.G.1` Distinguish between defining attributes (e.g., triangles are closed and three-sided) versus non- defining attributes (e.g., color, orientation, overall size); build and draw shapes to possess defining attributes.
- `1.G.2` Compose two-dimensional shapes (rectangles, squares, trapezoids, triangles, half circles, and quarter-circles) or three-dimensional shapes (cubes, right rectangular prisms, righ  circular cones, and right circular cylinders) to create a composite shape, and compose new s apes from the composite shape. (Students do not need to learn formal names such as “right rec angular prism.”
- `1.G.3` Partition circles and rectangles into two and four equal shares, describe the shares using the words _halves_ , _fourths_ , and _quarters_ , and use the phrases _half of_ , _fourth of_ , and _quarter of_. Describe the whole as two of, or four of the shares. Understand for these examples that decomposing into more equal shares creates smaller shares.

---

source: https://www.oregon.gov/ode/educator-resources/standards/mathematics/Documents/ccssm1.pdf
